#pragma once

#include <string>

class Task {
    public:
        Task(std::string name);
        std::string getName() const;

        virtual void operator()() = 0;

    private:
        std::string name;
};
