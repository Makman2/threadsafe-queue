#pragma once

template <typename T>
class MyQueueElement {
  public:
    MyQueueElement(T&& val) :
        next(nullptr),
        val(val)
    {}

    MyQueueElement* next;
    T val;
};
