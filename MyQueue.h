#pragma once

#include <condition_variable>
#include <cstddef>
#include <mutex>
#include <tuple>
#include "MyQueueElement.h"

template <typename T>
class MyQueue {
  public:
    typedef T value_type;
    typedef size_t size_type;

    MyQueue() :
        first(nullptr),
        last(nullptr),
        len(0)
    {}

    MyQueue(MyQueue<T> const& other) :
        MyQueue(this->copy_contents(other))
    {}

    MyQueue(MyQueue<T>&& other) :
        first(other.first),
        last(other.last),
        len(other.len)
    {
        other.first = nullptr;
        other.last = nullptr;
        other.len = 0;
    }

    ~MyQueue() {
        this->destruct_contents();
    }

    value_type const& front() const {
        // Undefined behavior when myqueue.size() == 0.
        const std::lock_guard<std::mutex> guard_this(this->write_lock);
        return this->first->val;
    }

    value_type& front() {
        // Undefined behavior when myqueue.size() == 0.
        const std::lock_guard<std::mutex> guard_this(this->write_lock);
        return this->first->val;
    }

    bool empty() const {
        return this->len == 0;
    }

    size_type size() const {
        return this->len;
    }

    template <typename... Args>
    void emplace(Args&&... args) {
        auto elem = new MyQueueElement<T>(std::move(T(std::forward<Args>(args)...)));

        {
            const std::lock_guard<std::mutex> guard(this->write_lock);

            this->len++;

            if (this->first == nullptr) {
                this->first = elem;
                this->last = elem;
                return;
            };

            this->last->next = elem;
            this->last = elem;
        }

        this->is_empty_condition.notify_one();
    }

    void push(const value_type& value) {
        this->emplace(std::move(T(value)));
    }
    void push(value_type&& value) {
        this->emplace(value);
    }

    value_type pop() {
        // Blocks if the queue is empty.

        MyQueueElement<T>* elem;

        { // This new scope is unnecessary, but to point out the critical section I kept it.
            std::unique_lock<std::mutex> lock(this->write_lock);
            this->is_empty_condition.wait(lock, [this] { return this->len > 0; });

            elem = this->first;
            this->first = elem->next;
            this->len--;
            if (this->first == nullptr) {
                this->last = nullptr;
            }
        }

        auto val = elem->val;
        delete elem;

        return val;
    }

    void swap(MyQueue<T>& other) {
        {
            const std::lock_guard<std::mutex> guard_other(other.write_lock);
            const std::lock_guard<std::mutex> guard_this(this->write_lock);

            std::swap(this->first, other.first);
            std::swap(this->last, other.last);
            std::swap(this->len, other.len);
        }

        this->is_empty_condition.notify_one();
    }

    MyQueue<T>& operator=(MyQueue<T> const& other) {
        std::pair<MyQueueElement<T>*, MyQueueElement<T>*> pointers;

        {
            const std::lock_guard<std::mutex> guard(other.write_lock);

            pointers = this->copy_contents(other);
        }

        {
            const std::lock_guard<std::mutex> guard(this->write_lock);

            this->destruct_contents();

            this->len = other.len;

            this->first = std::get<0>(pointers);
            this->last = std::get<1>(pointers);
        }

        this->is_empty_condition.notify_one();

        return this;
    }
    MyQueue<T>& operator=(MyQueue<T>&& other) {
        this->swap(other);
    }

  private:
    MyQueueElement<T>* first;
    MyQueueElement<T>* last;
    size_t len;

    MyQueue(std::tuple<MyQueueElement<T>*, MyQueueElement<T>*> pointers, size_type len) :
        first(std::get<0>(pointers)),
        last(std::get<1>(pointers)),
        len(len)
    {}

    void destruct_contents() {
        MyQueueElement<T>* elem = this->first;
        while (elem != nullptr) {
            auto const next = elem->next;
            delete elem;
            elem = next;
        }
    }

    static std::tuple<MyQueueElement<T>*, MyQueueElement<T>*> copy_contents(MyQueue<T> const& other) {
        if (other.first == nullptr) {
            return std::make_tuple(nullptr, nullptr);
        }

        MyQueueElement<T>* elem_other = other.first;
        MyQueueElement<T>* elem_this = new MyQueueElement<T>(T(*elem_other));
        auto first = elem_this;
        while (elem_other != nullptr) {
            elem_other = elem_other->next;
            elem_this->next = new MyQueueElement<T>(T(*elem_other));
            elem_this = elem_this->next;
        }
        auto last = elem_this;

        return std::make_tuple(first, last);
    }

    std::mutex write_lock;
    std::condition_variable is_empty_condition;
};
