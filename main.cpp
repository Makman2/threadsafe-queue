#include <condition_variable>
#include <sstream>
#include <iostream>
#include <thread>

#include "Task.h"
#include "MyQueue.h"

class MyFibonacciTask : public Task {
    public:
        MyFibonacciTask(int n):
            Task(MyFibonacciTask::format_name(n)),
            n(n) {}

        void operator()() override {
            int fibonacci;
            if (n <= 0) {
                fibonacci = 0;
            } else {
                int prev = 0;
                int next = 1;

                for (int i = 1; i < n; i++) {
                    auto intermediary = next;
                    next += prev;
                    prev = intermediary;
                }

                fibonacci = next;
            }

            std::cout << "fib(" << this->n << ") = " << fibonacci << std::endl;
        }

    private:
        int n;
        static std::string format_name(int n) {
            std::ostringstream stream;
            stream << "fib(" << n << ")";
            return stream.str();
        }
};

void produce(MyQueue<MyFibonacciTask>& queue) {
    while (true) {
        if (queue.size() < 5) {
            for (int i = 0; i < 20; i++) {
                queue.emplace(20);
            }
        }
    }
}

void consume(MyQueue<MyFibonacciTask>& queue) {
    while (true) {
        auto task = queue.pop();
        task();
    }
}

int main () {
    MyQueue<MyFibonacciTask> queue;

    std::thread producer(produce, std::ref(queue));
    std::thread consumer1(consume, std::ref(queue));
    std::thread consumer2(consume, std::ref(queue));

    producer.join();
    consumer1.join();
    consumer2.join();

    return 0;
}
