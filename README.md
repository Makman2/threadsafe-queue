# threadsafe-queue

A thread-safe queue implementation in C++.

## Compile

Compile with:

```bash
./compile.sh
```

Used C++ standard: C++17.

## Lint

Remove trailing spaces with:

```bash
./lint.sh
```
