#!/usr/bin/env bash
# Use -pthread for a more efficient implementation, this especially helps
# to mitigate busy-waiting for std::conditional_variable::wait!
g++ $(find -name "*.cpp") -pthread -o main.out
